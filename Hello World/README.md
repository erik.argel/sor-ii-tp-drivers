# Sistemas Operativos y Redes 2: Trabajo Práctico Char-Device

## Introducción

En este trabajo, se crea un char device de ejemplo basico.

### Codigo primerModulo.c

```c

#include <linux/module.h>
#include <linux/kernel.h>

int init_module ( void )
{ /* Constructor */
printk ( KERN_INFO "Admin: Driver registrado \n") ;
return 0;
}
void cleanup_module ( void )
{/* Destructor */
printk ( KERN_INFO "Admin: Driver desregistrado \n") ;
}
MODULE_LICENSE ("GPL") ;
MODULE_AUTHOR ("Argel Erik") ;
MODULE_DESCRIPTION ("Primer driver") ;


```
### Codigo Makefile

```shell

obj-m := primerModulo.o

all:
		make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
		make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean


```

Carpeta charDevice:

![imagenCDHW](imagenes/CapetaCharDeviceHelloWorld1.png)

Para compilara el codigo de nuestro device se utilizara el comando:

```shell
make clean & make
```

![imagenMCM](imagenes/Make_clean___make_Hello_World.png)

Resultado del comando anterior:

![imagenCDHW2](imagenes/CarpetaCharDeviceHelloWord2.png)

Ahora para cargar el modulo al kernel se utilizara el comando:

```shell
insmod modCharDev-TP.ko
```

![imagenIHW](imagenes/insmod_helloworld.png)

Luego utilizando el comando:

```shell
dmesg
```

Se puede apreciar los mensajes del nucleo (kernel), y al final de estos los generado al cargar el modulo. En este caso solo el mensaje de que el dirver se a registrado.

![imagenDMSHW](imagenes/dmesgHelloWorld.png)

Luego utilizando el comando:

```shell
modinfo primerModulo.ko
```

Se puede ver informacion sobre el modulo.

![imagenMIHW](imagenes/modinfoHelloWorld.png)

Por ultimo, para remover el modulo se utiliza el comando:

```shell
rmmod primerModulo.ko
```

![imagenRMMHW](imagenes/rmmodHelloWorld.png)

Y se puede ver con el comando:

```shell
dmesg
```

como se desregistra nuestro driver.

![imagenDMSHW2](imagenes/dmesgHelloWorld2.png)